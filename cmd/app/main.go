package main

import (
	"log"
	"os"
	"time"
)

// fake consumer app
func main() {
	heartbeat := time.NewTicker(2 * time.Second)

	heartattack := time.NewTimer(2 * time.Minute)

	for {
		select {
		case <-heartattack.C:
			log.Println("Heartbeat stopped /\\/\\---------")
			os.Exit(1)
		case heartbeatTime := <-heartbeat.C:
			log.Println("Heartbeat: -/\\/\\- ", heartbeatTime.Format(time.DateTime))
		}

	}
}
